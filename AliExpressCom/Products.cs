﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AliExpressCom
{
    class Products
    {
        private static Product[] _products = new Product[6];

        public static Product[] GetProducts()
        {
            _products[0] = new Product();
            _products[0].Name = "Samsung S10";
            _products[0].Price = 900;
            _products[0].Photo = AliExpressCom.Properties.Resources._1;
            _products[0].ID = 1;

            _products[1] = new Product();
            _products[1].Name = "Samsung S9";
            _products[1].Price = 800;
            _products[1].Photo = AliExpressCom.Properties.Resources._2;
            _products[1].ID = 2;

            _products[2] = new Product();
            _products[2].Name = "Samsung S8";
            _products[2].Price = 700;
            _products[2].Photo = AliExpressCom.Properties.Resources._3;
            _products[2].ID = 3;

            _products[3] = new Product();
            _products[3].Name = "Samsung S7";
            _products[3].Price = 600;
            _products[3].Photo = AliExpressCom.Properties.Resources._4;
            _products[3].ID = 4;

            _products[4] = new Product();
            _products[4].Name = "Samsung S6";
            _products[4].Price = 500;
            _products[4].Photo = AliExpressCom.Properties.Resources._5;
            _products[4].ID = 5;

            _products[5] = new Product();
            _products[5].Name = "Samsung S5";
            _products[5].Price = 400;
            _products[5].Photo = AliExpressCom.Properties.Resources._6;
            _products[5].ID = 6;


            return _products;
        }

        public static Product GetProductByID(int id)
        {
            Product[] products = GetProducts();

            //for(int i = 0; i < products.Length; i++)
            //{
            //    if (products[i].ID == id)
            //    {
            //        return products[i];
            //    }
            //}

            foreach (Product item in products)
            {
                if (item.ID == id)
                {
                    return item;
                }
            }
            return null;
        }



    }
}
