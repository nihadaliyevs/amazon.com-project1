﻿using AmazonCom.UseControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AmazonCom
{
    public partial class BasketForm : Form
    {
        public BasketForm()
        {
            InitializeComponent();
        }

        private void BasketForm_Load(object sender, EventArgs e)
        {
            int[] items =  Basket.ItemsInBasket();

            BasketProduct[] products= Products.GetProducts(items);
            

            int y = 40;
            decimal sum = 0;
                foreach(BasketProduct product in products)
            {
                if (product != null)
                {
                    BasketLineControl basketLineControl = new BasketLineControl();
                    basketLineControl.lbl_productName.Text = product.Product.Name;
                    basketLineControl.lbl_ProductCount.Text = product.Count.ToString();
                    basketLineControl.lbl_productSum.Text = product.Product.Price.ToString();
                    basketLineControl.lbl_productTotalSum.Text = product.TotalSum.ToString();
                    sum += product.TotalSum;
                    basketLineControl.Location = new Point(30, y);
                    basketLineControl.btn_add.Click += Btn_add_Click;
                    basketLineControl.btn_add.Cursor = Cursors.Hand;
                    basketLineControl.btn_remove.Click += Btn_remove_Click;
                    basketLineControl.btn_remove.Cursor = Cursors.Hand;
                    Controls.Add(basketLineControl);
                    y += 100;
                }
              }
            lbl_TotalSum.Text = sum.ToString();

        }

        private void Btn_remove_Click(object sender, EventArgs e)
        {
            BasketLineControl basketLineControl = (sender as Button).Parent as BasketLineControl;

            decimal itemCount = (decimal.Parse(basketLineControl.lbl_ProductCount.Text)-1);
            decimal itemSum = decimal.Parse(basketLineControl.lbl_productSum.Text);

            if (itemCount == 0)
            {
                basketLineControl.Parent.Controls.Remove(basketLineControl);
            }
            else
            {
                basketLineControl.lbl_ProductCount.Text = itemCount.ToString();
                
                basketLineControl.lbl_productTotalSum.Text = (itemCount * itemSum).ToString();

            }
            ModifySum(itemSum, '-');
        }

        private void ModifySum(decimal price, char symbol)
        {
            decimal totalPrice = decimal.Parse(lbl_TotalSum.Text);
            if (symbol == '+')
            {
                lbl_TotalSum.Text = (totalPrice + price).ToString();
            }
            else
            {
                lbl_TotalSum.Text = (totalPrice - price).ToString();
            }
        }

        private void Btn_add_Click(object sender, EventArgs e)
        {
            BasketLineControl basketLineControl= (sender as Button).Parent as BasketLineControl;
            basketLineControl.lbl_ProductCount.Text = (decimal.Parse(basketLineControl.lbl_ProductCount.Text)+1).ToString();
            basketLineControl.lbl_productTotalSum.Text = (decimal.Parse(basketLineControl.lbl_ProductCount.Text) * decimal.Parse(basketLineControl.lbl_productSum.Text)).ToString();
            ModifySum(decimal.Parse(basketLineControl.lbl_productSum.Text), '+');
        }
    }
}
