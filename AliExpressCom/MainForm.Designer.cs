﻿namespace AliExpressCom
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_basket = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_basket
            // 
            this.btn_basket.BackgroundImage = global::AliExpressCom.Properties.Resources.iconfinder_shopping_basket_1608413;
            this.btn_basket.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_basket.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_basket.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_basket.ForeColor = System.Drawing.Color.MediumBlue;
            this.btn_basket.Location = new System.Drawing.Point(2336, 53);
            this.btn_basket.Name = "btn_basket";
            this.btn_basket.Size = new System.Drawing.Size(279, 151);
            this.btn_basket.TabIndex = 0;
            this.btn_basket.Text = "0";
            this.btn_basket.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.btn_basket.UseVisualStyleBackColor = true;
            this.btn_basket.Click += new System.EventHandler(this.btn_basket_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(2645, 1494);
            this.Controls.Add(this.btn_basket);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_basket;
    }
}