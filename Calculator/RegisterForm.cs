﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    public partial class RegisterForm : Form
    {
        public RegisterForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void txtbx_FirstNumber_TextChanged(object sender, EventArgs e)
        {

        }

        private void btn_Calculate_Click(object sender, EventArgs e)
        {
            Calculator calculator = new Calculator();
            calculator.FirstNumber = Convert.ToInt32( txtbx_FirstNumber.Text);
            calculator.SecondNumber = Convert.ToInt32(txtbx_SecondNumber.Text);
            calculator.operation = Convert.ToChar(cmbx_Operation.Text);
            int result= calculator.Calculate();
            MessageBox.Show($"{txtbx_FirstNumber.Text}{cmbx_Operation.Text}{txtbx_SecondNumber.Text} = "+result.ToString());
            
        }
    }
}
