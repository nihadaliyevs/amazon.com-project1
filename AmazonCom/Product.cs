﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazonCom
{
    class Product
    {
        private string _name;
        private decimal _price;
        private Image _photo;
        private int _id;

        public String Name
        {
            set { _name = value; }
            get { return _name; }
        }

        public decimal Price
        {
            set { _price = value; }
            get { return _price; }
        }
         public Image Photo
        {
            set { _photo = value; }
            get { return _photo; }
        }
        public int ID
        {
            set { _id = value; }
            get { return _id; }
        }


    }
}
