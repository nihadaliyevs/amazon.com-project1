﻿namespace AmazonCom.UseControls
{
    partial class BasketLineControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_productName = new System.Windows.Forms.Label();
            this.lbl_ProductCount = new System.Windows.Forms.Label();
            this.lbl_productSum = new System.Windows.Forms.Label();
            this.lbl_productTotalSum = new System.Windows.Forms.Label();
            this.btn_add = new System.Windows.Forms.Button();
            this.btn_remove = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_productName
            // 
            this.lbl_productName.AutoSize = true;
            this.lbl_productName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.900001F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_productName.Location = new System.Drawing.Point(4, 37);
            this.lbl_productName.Name = "lbl_productName";
            this.lbl_productName.Size = new System.Drawing.Size(0, 39);
            this.lbl_productName.TabIndex = 0;
            // 
            // lbl_ProductCount
            // 
            this.lbl_ProductCount.AutoSize = true;
            this.lbl_ProductCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.900001F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_ProductCount.Location = new System.Drawing.Point(506, 37);
            this.lbl_ProductCount.Name = "lbl_ProductCount";
            this.lbl_ProductCount.Size = new System.Drawing.Size(0, 39);
            this.lbl_ProductCount.TabIndex = 1;
            this.lbl_ProductCount.Click += new System.EventHandler(this.lbl_ProductCount_Click);
            // 
            // lbl_productSum
            // 
            this.lbl_productSum.AutoSize = true;
            this.lbl_productSum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.900001F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_productSum.Location = new System.Drawing.Point(787, 37);
            this.lbl_productSum.Name = "lbl_productSum";
            this.lbl_productSum.Size = new System.Drawing.Size(0, 39);
            this.lbl_productSum.TabIndex = 2;
            // 
            // lbl_productTotalSum
            // 
            this.lbl_productTotalSum.AutoSize = true;
            this.lbl_productTotalSum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.900001F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_productTotalSum.Location = new System.Drawing.Point(1069, 37);
            this.lbl_productTotalSum.Name = "lbl_productTotalSum";
            this.lbl_productTotalSum.Size = new System.Drawing.Size(0, 39);
            this.lbl_productTotalSum.TabIndex = 3;
            this.lbl_productTotalSum.Click += new System.EventHandler(this.lbl_productTotalSum_Click);
            // 
            // btn_add
            // 
            this.btn_add.Location = new System.Drawing.Point(1335, 31);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(63, 55);
            this.btn_add.TabIndex = 4;
            this.btn_add.Text = "+";
            this.btn_add.UseVisualStyleBackColor = true;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // btn_remove
            // 
            this.btn_remove.Location = new System.Drawing.Point(1425, 31);
            this.btn_remove.Name = "btn_remove";
            this.btn_remove.Size = new System.Drawing.Size(63, 55);
            this.btn_remove.TabIndex = 5;
            this.btn_remove.Text = "-";
            this.btn_remove.UseVisualStyleBackColor = true;
            this.btn_remove.Click += new System.EventHandler(this.btn_remove_Click);
            // 
            // BasketLineControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btn_remove);
            this.Controls.Add(this.btn_add);
            this.Controls.Add(this.lbl_productTotalSum);
            this.Controls.Add(this.lbl_productSum);
            this.Controls.Add(this.lbl_ProductCount);
            this.Controls.Add(this.lbl_productName);
            this.Name = "BasketLineControl";
            this.Size = new System.Drawing.Size(1754, 113);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label lbl_productName;
        public System.Windows.Forms.Label lbl_ProductCount;
        public System.Windows.Forms.Label lbl_productSum;
        public System.Windows.Forms.Label lbl_productTotalSum;
        public System.Windows.Forms.Button btn_add;
        public System.Windows.Forms.Button btn_remove;
    }
}
