﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AliExpressCom
{
    public partial class RegisterForm : Form
    {
        public RegisterForm()
        {
            InitializeComponent();
        }

        private void btn_register_Click(object sender, EventArgs e)
        {
            lbl_NameError.Text = "";
            lbl_EmailError.Text = "";
            lbl_passwordError.Text = "";
            lbl_confirmPasswordError.Text = "";

            bool isValidationOk = true;

            if (String.IsNullOrWhiteSpace(txtbx_Name.Text) ||txtbx_Name.Text.Length<2)
            {
                isValidationOk = false;
                lbl_NameError.Text = "Name must have at least 2 sybols";
            }
            if (String.IsNullOrWhiteSpace(txtbx_Email.Text) || !txtbx_Email.Text.Contains("@")||!txtbx_Email.Text.Contains(".")||txtbx_Email.Text.Length<8)
            {
                isValidationOk = false;
                lbl_EmailError.Text = "Email is not valid!";
            }
            if (String.IsNullOrWhiteSpace(txtbx_password.Text) || txtbx_password.Text.Length < 6)
            {
                isValidationOk = false;
                lbl_passwordError.Text = "Password must have at least 6 symbols";
            }
            if (string.IsNullOrWhiteSpace(txtbx_ConfirmPassword.Text) || txtbx_password.Text != txtbx_ConfirmPassword.Text)
            {
                isValidationOk = false;
                lbl_confirmPasswordError.Text = "Please reenter your password above!";
            }
            User user = new User();
            user.Name = txtbx_Name.Text;
            user.Email = txtbx_Email.Text;
            user.Password = txtbx_password.Text;
            
            
            if (isValidationOk)
            {
                Hide();
                Users.Add(user);
                LoginForm login = new LoginForm();
                login.ShowDialog();

            }

        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            Hide();
            GreetingForm greetingForm = new GreetingForm();
            greetingForm.ShowDialog();
        }
    }
}
