﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazonCom
{
    class Products
    {
        private static Product[] _products = new Product[6];

        public static Product[] GetProducts()
        {
            _products[0] = new Product();
            _products[0].Name = "Samsung S10";
            _products[0].Price = 600;
            _products[0].Photo = AmazonCom.Properties.Resources._1;
            _products[0].ID = 1;

            _products[1] = new Product();
            _products[1].Name = "Samsung S9";
            _products[1].Price = 500;
            _products[1].Photo = AmazonCom.Properties.Resources._2;
            _products[1].ID = 2;

            _products[2] = new Product();
            _products[2].Name = "Samsung S8";
            _products[2].Price = 400;
            _products[2].Photo = AmazonCom.Properties.Resources._3;
            _products[2].ID = 3;

            _products[3] = new Product();
            _products[3].Name = "Samsung S7";
            _products[3].Price = 300;
            _products[3].Photo = AmazonCom.Properties.Resources._4;
            _products[3].ID = 4;

            _products[4] = new Product();
            _products[4].Name = "Samsung S6";
            _products[4].Price = 200;
            _products[4].Photo = AmazonCom.Properties.Resources._5;
            _products[4].ID = 5;

            _products[5] = new Product();
            _products[5].Name = "Samsung S5";
            _products[5].Price = 100;
            _products[5].Photo = AmazonCom.Properties.Resources._6;
            _products[5].ID = 6;

            return _products;
        }

        public static Product GetProductById(int id)// Colden gelen id-ni movcud productlarin icinde yoxlayir eger bele id varsa
                                                    //hemin id-nin aid oldugu producti qaytarir
        {
            Product[] products = GetProducts(); //butun productlari cagirir
            foreach (Product item in products) //butun producrlarin icinde bir-bir gezir
            {
                if (item.ID == id) //eger movcud productlardan birinin id-si colden gelen id-ye beraberdirse, hemin producti qaytarir
                {
                    return item;
                }
            }
            return null;//metod if funksiyasiyasina girmese null qaytarir.
        }

        public static BasketProduct[] GetProducts(int[] items)
        {

            BasketProduct[] bProducts = new BasketProduct[30];//metod bir productlar massivi qaytaracaq. bu hemin massivdir.

            // Product[] existance = GetProducts(); Ehtiyac qalmadi
            int counter = 0;

            for (int i = 0; i < items.Length; i++)//for dovru qurulur. colden gelen productlarin her birinin i-ci elementi asagidaki
                                                  //kimi yoxlanilir
            {
                Product product = GetProductById(items[i]);
                BasketProduct basketProduct = new BasketProduct();
                basketProduct.Product = product;
                Basket.AddToBasket(basketProduct);
                
            }
            return Basket.GetAllBasketProducts();
        }

       
        
    }
}
