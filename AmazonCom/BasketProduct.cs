﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazonCom
{
    class BasketProduct
    {
        public Product Product { get; set; }
        public int Count { get; set; }
        public decimal TotalSum { get; set; }
    }
}
