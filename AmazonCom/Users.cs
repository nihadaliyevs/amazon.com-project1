﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazonCom
{
    public static class Users
    {
        
        private static User[] _user = new User[10]  ;
        private static int counter = 0;
        public static void Add(User user)
        {
                _user[counter] = user;
                counter++;
            
        }

        public static User[] GetUsers()
        {
            return _user;
        }

        public static bool FindUser(string email, string password)
        {
            bool isFound = false;
            for(int i=0; i <counter; i++)
            {
                if(_user[i].Email==email && _user[i].Password == password)
                {
                    isFound = true;
                    break;
                }
             }
            return isFound;
        }
    }
}
