﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AmazonCom
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }
        private void SelectImage(int i, PictureBox pictureBox)
        {
            Product[] products = Products.GetProducts();
            pictureBox.Image = products[i - 1].Photo;
            
        }
        private void MainForm_Load(object sender, EventArgs e)
        {
            int x = 47;
            int y = 39;
            int nameY = 190;
            int pricey = 215;

            Product[] products = Products.GetProducts();

            for (int i = 1; i <= products.Length; i++)
            {
                if (i % 4 == 0)
                {
                    x = 47;
                    y += 230;
                    nameY += 240;
                    pricey += 245;
                }

                PictureBox pictureBox1 = new PictureBox();
                pictureBox1.Image = products[i - 1].Photo;
                pictureBox1.Size = new Size(174, 144);
                pictureBox1.Location = new Point(x, y);
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;

                Label labelName = new Label();
                labelName.Text = $"Name:  {products[i - 1].Name}";
              //  labelName.Text += products[i-1].Name;
                labelName.Location = new Point(x, nameY);

                Label labelPrice = new Label();
                labelPrice.Text = $"Price:  {products[i - 1].Price.ToString()}";
                //labelPrice.Text += products[i-1].Price.ToString();
                labelPrice.Location = new Point(x, pricey);
                labelPrice.Width = 90;

                Button button = new Button();
                button.Text = "Add";
                button.Location = new Point(x + 90, pricey);
                button.Cursor = Cursors.Hand;
                button.Click += Button_Click;
                button.Tag = products[i-1].ID;

                Controls.Add(pictureBox1);
                Controls.Add(labelName);
                Controls.Add(labelPrice);
                Controls.Add(button);
                x += 240;
 
            }
           
        }

        private void Button_Click(object sender, EventArgs e)
        {
            //Button btn = sender as Button;// bu kodu bele de yazmaq olar Button btn = (Button)sender; biz yazdigimizin ustunluyu odur ki, cevrilme bas vermezse null qaytaracaq.
            //MessageBox.Show(btn.Tag.ToString());
            //lbl_basket_item_count.Text = (int.Parse(lbl_basket_item_count.Text) + 1).ToString();
            Button btn = sender as Button;
            Basket.AddToBasket(int.Parse(btn.Tag.ToString())); //hər dəfə add buttonuna click olduqda 
                                                               //basket clasının içində yaranan massivə buttondan gələn id-lər dolur.
            //Basket.AddToBasket(btn.Tag); - yazmaq olmur. Çünki obyekti int-ə çevirmək olmur. Obyekti strinqə çevirib, oradan int-ə çevirmək olur
            lbl_basket_item_count.Text = Basket.ItemsCountInBasket().ToString();
        }

        private void lbl_basket_item_count_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            BasketForm basketForm = new BasketForm();
            basketForm.ShowDialog();
        }
    }
}
