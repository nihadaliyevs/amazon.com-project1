﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazonCom
{
    static class Basket
    {
        static int[] _productsId = new int[30];
        static int _counter = 0;

        public static void AddToBasket(int id)
        {
            _productsId[_counter] = id;//MainFormda Add Buttona hər dəfə click edəndə bu funksiya düşəcək. Həmin buttonun tagına Malın id-ni yazacağıq
                                        //funksiya çağırılanda tag gəlib id olacaq və yuxarıda yaratdığımız massivin hər bir elementinə basılan 
                                        //id yazılacaq. hər dəfə də counter bir vahid çoxalır. 

            _counter++;
        }

        public static int ItemsCountInBasket()//bu metodun məqsədi odur ki, sağ tərəfdə basketə nə qədər mal əlavə edildiyini göstərsin. 
        {
            return _counter;
        }

        public static int[] ItemsInBasket()//bu metodun məqsədi ondan ibarətdir ki, 11-ci sətrdə baskertə yerləşən malların həcmi qədər bir massiv yaranıb. 
                                            //bu massiv isə faktiki basketdə olan malların həcmini göstərir. 
        {
            int[] ms = new int[_counter];//göründüyü kimi massivin uzunluğu 20-ci sətrdə artan _counterin uzunluğu qədərdir. 
            for (int i = 0; i < _counter; i++)
            {
                ms[i] = _productsId[i];
            }
            return ms;
        }





        static BasketProduct[] _basketProducts = new BasketProduct[30];//_basketproduct deye bir massiv yaranir
        static int _bcounter = 0;

        public static BasketProduct HasInBasket(int id) 
        {
            foreach (BasketProduct item in _basketProducts)
            {
                if(item !=null && item.Product.ID == id)
                {
                    return item;
                }
            }
            return null;
        }

        public static void AddToBasket(BasketProduct basketProduct)
        {
            BasketProduct product = HasInBasket(basketProduct.Product.ID);//deməli hasinbasket metoduna bir basketproduct tipində item-in productının id-ni göndəririk
                                                                          //əgər belə item olmasa, o zaman yeni yaranan productın countun 1 vahid artırıb 
                                                                          //totalsum tapırıq. Əks halda _basketProducts[_bcounter-sıfırıncı elementinə] 
                                                                           //çöldən gələn basketProductı təyin edirik. Countunu 1 edirik və totalsum-ı gələnin price-ni təyin edirik.
            if (product != null)
            {
                product.Count++;
                product.TotalSum = product.Count * product.Product.Price;
            }
            else
            {
                _basketProducts[_bcounter] = basketProduct;
                _basketProducts[_bcounter].Count = 1;
                _basketProducts[_bcounter].TotalSum = basketProduct.Product.Price;
                _bcounter++;
            }
        }

        public static BasketProduct[] GetAllBasketProducts()
        {
            return _basketProducts;
        }

    }
}
