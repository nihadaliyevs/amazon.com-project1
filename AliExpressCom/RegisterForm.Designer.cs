﻿namespace AliExpressCom
{
    partial class RegisterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtbx_Name = new System.Windows.Forms.TextBox();
            this.txtbx_Email = new System.Windows.Forms.TextBox();
            this.txtbx_password = new System.Windows.Forms.TextBox();
            this.txtbx_ConfirmPassword = new System.Windows.Forms.TextBox();
            this.lbl_NameError = new System.Windows.Forms.Label();
            this.lbl_EmailError = new System.Windows.Forms.Label();
            this.lbl_passwordError = new System.Windows.Forms.Label();
            this.lbl_confirmPasswordError = new System.Windows.Forms.Label();
            this.btn_register = new System.Windows.Forms.Button();
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(326, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(327, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "Please enter your Name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(326, 570);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(401, 32);
            this.label2.TabIndex = 1;
            this.label2.Text = "Please confirm your password:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(326, 401);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(373, 32);
            this.label3.TabIndex = 2;
            this.label3.Text = "Please enter your password:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(326, 222);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(321, 32);
            this.label4.TabIndex = 3;
            this.label4.Text = "Please enter your email:";
            // 
            // txtbx_Name
            // 
            this.txtbx_Name.Location = new System.Drawing.Point(332, 61);
            this.txtbx_Name.Name = "txtbx_Name";
            this.txtbx_Name.Size = new System.Drawing.Size(395, 38);
            this.txtbx_Name.TabIndex = 4;
            // 
            // txtbx_Email
            // 
            this.txtbx_Email.Location = new System.Drawing.Point(332, 257);
            this.txtbx_Email.Name = "txtbx_Email";
            this.txtbx_Email.Size = new System.Drawing.Size(395, 38);
            this.txtbx_Email.TabIndex = 5;
            // 
            // txtbx_password
            // 
            this.txtbx_password.Location = new System.Drawing.Point(332, 436);
            this.txtbx_password.Name = "txtbx_password";
            this.txtbx_password.Size = new System.Drawing.Size(395, 38);
            this.txtbx_password.TabIndex = 6;
            // 
            // txtbx_ConfirmPassword
            // 
            this.txtbx_ConfirmPassword.Location = new System.Drawing.Point(332, 605);
            this.txtbx_ConfirmPassword.Name = "txtbx_ConfirmPassword";
            this.txtbx_ConfirmPassword.Size = new System.Drawing.Size(395, 38);
            this.txtbx_ConfirmPassword.TabIndex = 7;
            // 
            // lbl_NameError
            // 
            this.lbl_NameError.AutoSize = true;
            this.lbl_NameError.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.900001F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_NameError.ForeColor = System.Drawing.Color.DarkRed;
            this.lbl_NameError.Location = new System.Drawing.Point(332, 106);
            this.lbl_NameError.Name = "lbl_NameError";
            this.lbl_NameError.Size = new System.Drawing.Size(0, 39);
            this.lbl_NameError.TabIndex = 8;
            // 
            // lbl_EmailError
            // 
            this.lbl_EmailError.AutoSize = true;
            this.lbl_EmailError.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.900001F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_EmailError.ForeColor = System.Drawing.Color.DarkRed;
            this.lbl_EmailError.Location = new System.Drawing.Point(332, 298);
            this.lbl_EmailError.Name = "lbl_EmailError";
            this.lbl_EmailError.Size = new System.Drawing.Size(0, 39);
            this.lbl_EmailError.TabIndex = 9;
            // 
            // lbl_passwordError
            // 
            this.lbl_passwordError.AutoSize = true;
            this.lbl_passwordError.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.900001F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_passwordError.ForeColor = System.Drawing.Color.DarkRed;
            this.lbl_passwordError.Location = new System.Drawing.Point(332, 477);
            this.lbl_passwordError.Name = "lbl_passwordError";
            this.lbl_passwordError.Size = new System.Drawing.Size(0, 39);
            this.lbl_passwordError.TabIndex = 10;
            // 
            // lbl_confirmPasswordError
            // 
            this.lbl_confirmPasswordError.AutoSize = true;
            this.lbl_confirmPasswordError.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.900001F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_confirmPasswordError.ForeColor = System.Drawing.Color.DarkRed;
            this.lbl_confirmPasswordError.Location = new System.Drawing.Point(332, 665);
            this.lbl_confirmPasswordError.Name = "lbl_confirmPasswordError";
            this.lbl_confirmPasswordError.Size = new System.Drawing.Size(0, 39);
            this.lbl_confirmPasswordError.TabIndex = 11;
            // 
            // btn_register
            // 
            this.btn_register.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_register.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_register.Font = new System.Drawing.Font("Modern No. 20", 9.9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_register.Location = new System.Drawing.Point(189, 736);
            this.btn_register.Name = "btn_register";
            this.btn_register.Size = new System.Drawing.Size(283, 99);
            this.btn_register.TabIndex = 12;
            this.btn_register.Text = "Register";
            this.btn_register.UseVisualStyleBackColor = false;
            this.btn_register.Click += new System.EventHandler(this.btn_register_Click);
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_Cancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Cancel.Font = new System.Drawing.Font("Modern No. 20", 9.9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Cancel.Location = new System.Drawing.Point(559, 736);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(283, 99);
            this.btn_Cancel.TabIndex = 13;
            this.btn_Cancel.Text = "Cancel";
            this.btn_Cancel.UseVisualStyleBackColor = false;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // RegisterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1109, 1196);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.btn_register);
            this.Controls.Add(this.lbl_confirmPasswordError);
            this.Controls.Add(this.lbl_passwordError);
            this.Controls.Add(this.lbl_EmailError);
            this.Controls.Add(this.lbl_NameError);
            this.Controls.Add(this.txtbx_ConfirmPassword);
            this.Controls.Add(this.txtbx_password);
            this.Controls.Add(this.txtbx_Email);
            this.Controls.Add(this.txtbx_Name);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "RegisterForm";
            this.Text = "RegisterForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtbx_Name;
        private System.Windows.Forms.TextBox txtbx_Email;
        private System.Windows.Forms.TextBox txtbx_password;
        private System.Windows.Forms.TextBox txtbx_ConfirmPassword;
        private System.Windows.Forms.Label lbl_NameError;
        private System.Windows.Forms.Label lbl_EmailError;
        private System.Windows.Forms.Label lbl_passwordError;
        private System.Windows.Forms.Label lbl_confirmPasswordError;
        private System.Windows.Forms.Button btn_register;
        private System.Windows.Forms.Button btn_Cancel;
    }
}