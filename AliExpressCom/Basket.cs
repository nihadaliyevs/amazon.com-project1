﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AliExpressCom
{
    static class Basket
    {

        //MainFormda istifadə ediləcək Komponentlər:

        static int[] productsId = new int[30];
        static int _counter = 0;

        public static void AddToBasket(int id)
        {
            
                productsId[_counter] = id;
                _counter++;
            
        }

        public static int ItemCountsInBasket()
        {
            return _counter;

        }

        //BasketFormda istifadə edilcək componentlər:

        public static int[] ItemsInBasket()
        {
            int[] ms = new int[_counter];

            for(int i=0; i <= _counter; i++)
            {
                ms[i] = productsId[i];
            }
            return ms;
        }

        static BasketProduct[] _basketProducts = new BasketProduct[30];
        static int _bcounter = 0;

        public static BasketProduct HasInBasket(int id)
        {
            foreach (BasketProduct item in _basketProducts)
            {
                if(item!=null && item.Product.ID == id)
                {
                    return item;
                }
            }
            return null;
        }

        public static void AddToBasket(BasketProduct basketProduct)
        {
            BasketProduct product = HasInBasket(basketProduct.Product.ID);

            if (product != null)
            {
                product.Count++;
                product.TotalSum = product.Count * product.Product.Price;
            }
            else
            {
                _basketProducts[_bcounter] = basketProduct;
                _basketProducts[_bcounter].Count = 1;
                _basketProducts[_bcounter].TotalSum = basketProduct.Product.Price;
                _bcounter++;
            }
         }
       
       

        

    }
}
