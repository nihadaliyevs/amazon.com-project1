﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AliExpressCom
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            int x = 47;
            int y = 39;
            int namey = 200;
            int pricey = 230;


            Product[] products = Products.GetProducts();

            for(int i=0; i<products.Length; i++)
            {
                if ((i+1) % 4 == 0)
                {
                    x = 47;
                    y += 230;
                    namey += 240;
                    pricey += 240;
                }

                PictureBox pictureBox1 = new PictureBox();
                pictureBox1.Size = new Size(174, 144);
                pictureBox1.Location = new Point(x, y);
                pictureBox1.Image = products[i].Photo;
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
               

                Label labelName = new Label();
                labelName.Text = products[i].Name;
                labelName.Location = new Point(x, namey);

                Label labelPrice = new Label();
                labelPrice.Text = $"Price : {products[i].Price.ToString()}";
                labelPrice.Location = new Point(x, pricey);

                Button button = new Button();
                button.Text = "Add";
                button.Location = new Point(x+110, pricey-5);
                button.Size = new Size(50, 25);
                button.Click += Button_Click;
                button.Tag = products[i].ID;
                button.Cursor = Cursors.Hand;
                x += 240;

                Controls.Add(pictureBox1);
                Controls.Add(labelName);
                Controls.Add(labelPrice);
                Controls.Add(button);
            }
        }

        private void Button_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;

            Basket.AddToBasket(int.Parse(btn.Tag.ToString()));

            btn_basket.Text = "";
            btn_basket.Text = Basket.ItemCountsInBasket().ToString();
            btn_basket.Cursor = Cursors.Hand;


        }

        private void btn_basket_Click(object sender, EventArgs e)
        {
            BasketForm basketForm = new BasketForm();
            basketForm.Show();
            
        }
    }
}
