﻿namespace AmazonCom
{
    partial class MainForm
    {
        
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_basket_item_count = new System.Windows.Forms.LinkLabel();
            this.btn_basket = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_basket_item_count
            // 
            this.lbl_basket_item_count.AutoSize = true;
            this.lbl_basket_item_count.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_basket_item_count.Location = new System.Drawing.Point(2163, 78);
            this.lbl_basket_item_count.Name = "lbl_basket_item_count";
            this.lbl_basket_item_count.Size = new System.Drawing.Size(42, 46);
            this.lbl_basket_item_count.TabIndex = 1;
            this.lbl_basket_item_count.TabStop = true;
            this.lbl_basket_item_count.Text = "0";
            this.lbl_basket_item_count.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lbl_basket_item_count_LinkClicked);
            // 
            // btn_basket
            // 
            this.btn_basket.BackgroundImage = global::AmazonCom.Properties.Resources.iconfinder_shopping_basket_1608413;
            this.btn_basket.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_basket.Location = new System.Drawing.Point(2063, 37);
            this.btn_basket.Name = "btn_basket";
            this.btn_basket.Size = new System.Drawing.Size(94, 87);
            this.btn_basket.TabIndex = 0;
            this.btn_basket.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(2223, 1492);
            this.Controls.Add(this.lbl_basket_item_count);
            this.Controls.Add(this.btn_basket);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_basket;
        private System.Windows.Forms.LinkLabel lbl_basket_item_count;
    }
}