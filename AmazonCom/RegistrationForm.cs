﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AmazonCom
{
    public partial class RegistrationForm : Form
    {
        public RegistrationForm()
        {
            InitializeComponent();
        }

        private void btn_register_Click(object sender, EventArgs e)
        {
            lbl_nameError.Text = "";
            lbl_emailError.Text = "";
            lbl_passwordError.Text = "";
            lbl_confirmPasswordError.Text = "";

            //name can't be null or whitespace and must have at least 2 symbols
            //email must have at least 6 symbols and @
            //pasword must have at least 6 sybols
            //confirm password must be equal to password
            bool isvalidationOk = true;

            if (string.IsNullOrWhiteSpace(txtbx_name.Text) || txtbx_name.Text.Length < 2)
            {
                isvalidationOk = false;
                lbl_nameError.Text = "Name must have at least 2 symbols";
            }
            if(string.IsNullOrWhiteSpace(txtxbx_email.Text)|| !txtxbx_email.Text.Contains("@"))
            {
                lbl_emailError.Text = "invalid email";
                isvalidationOk = false;
            }
            if (string.IsNullOrWhiteSpace(txtbx_password.Text) || txtbx_password.Text.Length < 6)
            {
                lbl_passwordError.Text = "Password must have at least 6 characters";
                isvalidationOk = false;
            }
            if (txtbx_passwordConfirm.Text != txtbx_password.Text)
            {
                lbl_confirmPasswordError.Text = "Please enter the password again and match the password";
                isvalidationOk = false;
            }

            if (isvalidationOk)
            {
                this.Hide();
                //User user = new User();
                //user.Name = txtbx_name.Text;
                //user.Email = txtxbx_email.Text;
                //user.Password = txtbx_password.Text;
                

                //Users.Add(user);
                

                LoginForm loginForm = new LoginForm();
                loginForm.ShowDialog();
                
            }
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            Hide();
            GreetingForm greetingForm = new GreetingForm();
            greetingForm.ShowDialog();
        }

        private void RegistrationForm_Load(object sender, EventArgs e)
        {

        }
    }
}
