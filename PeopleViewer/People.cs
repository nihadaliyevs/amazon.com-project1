﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeopleViewer
{
    class People
    {
        private static Person[] people = new Person[15];

        public static Person [] AllPeople()
        {
            people[0] = new Person();
            people[0].Name = "Tural";
            people[0].Surname = "Suleymani";
            people[0].Email = "turalsuleymani@gmail.com";
            people[0].Photo = PeopleViewer.Properties.Resources.person;
            people[0].RegistrationNumber = 1;

            people[1] = new Person();
            people[1].Name = "Nihad";
            people[1].Surname = "Aliyev";
            people[1].Email = "nihadaliyevs@gmail.com";
            people[1].Photo = PeopleViewer.Properties.Resources.person;
            people[1].RegistrationNumber = 2;

            people[2] = new Person();
            people[2].Name = "Nurlana";
            people[2].Surname = "Mammadova";
            people[2].Email = "nirlana@gmail.com";
            people[2].Photo = PeopleViewer.Properties.Resources.person;
            people[2].RegistrationNumber = 3;

            people[3] = new Person();
            people[3].Name = "Zarifa";
            people[3].Surname = "Hasanova";
            people[3].Email = "zarifa@gmail.com";
            people[3].Photo = PeopleViewer.Properties.Resources.person;
            people[3].RegistrationNumber = 4;

            people[4] = new Person();
            people[4].Name = "Rahib";
            people[4].Surname = "Veliyev";
            people[4].Email = "rahib@gmail.com";
            people[4].Photo = PeopleViewer.Properties.Resources.person;
            people[4].RegistrationNumber = 5;

            people[5] = new Person();
            people[5].Name = "Musa";
            people[5].Surname = "Musayev";
            people[5].Email = "musa@gmail.com";
            people[5].Photo = PeopleViewer.Properties.Resources.person;
            people[5].RegistrationNumber = 6;

            people[6] = new Person();
            people[6].Name = "Rza";
            people[6].Surname = "Rzayev";
            people[6].Email = "rza@gmail.com";
            people[6].Photo = PeopleViewer.Properties.Resources.person;
            people[6].RegistrationNumber = 7;

            people[7] = new Person();
            people[7].Name = "Murad";
            people[7].Surname = "Muradov";
            people[7].Email = "murad@gmail.com";
            people[7].Photo = PeopleViewer.Properties.Resources.person;
            people[7].RegistrationNumber = 8;

            people[8] = new Person();
            people[8].Name = "Isa";
            people[8].Surname = "isayev";
            people[8].Email = "isa@gmail.com";
            people[8].Photo = PeopleViewer.Properties.Resources.person;
            people[8].RegistrationNumber = 9;

            people[9] = new Person();
            people[9].Name = "Ali";
            people[9].Surname = "Aliyev";
            people[9].Email = "ali@gmail.com";
            people[9].Photo = PeopleViewer.Properties.Resources.person;
            people[9].RegistrationNumber = 10;

            people[10] = new Person();
            people[10].Name = "Vali";
            people[10].Surname = "Valiyev";
            people[10].Email = "Vali@gmail.com";
            people[10].Photo = PeopleViewer.Properties.Resources.person;
            people[10].RegistrationNumber = 11;

            people[11] = new Person();
            people[11].Name = "Leyla";
            people[11].Surname = "Badalova";
            people[11].Email = "Leyla@gmail.com";
            people[11].Photo = PeopleViewer.Properties.Resources.person;
            people[11].RegistrationNumber = 12;

            people[12] = new Person();
            people[12].Name = "Samir";
            people[12].Surname = "Dadashzade";
            people[12].Email = "samir@gmail.com";
            people[12].Photo = PeopleViewer.Properties.Resources.person;
            people[12].RegistrationNumber = 13;

            people[13] = new Person();
            people[13].Name = "Kamil";
            people[13].Surname = "Shamilzade";
            people[13].Email = "turalsuleymani@gmail.com";
            people[13].Photo = PeopleViewer.Properties.Resources.person;
            people[13].RegistrationNumber = 14;

            people[14] = new Person();
            people[14].Name = "Mirad";
            people[14].Surname = "Ibrahimxanli";
            people[14].Email = "murad@gmail.com";
            people[14].Photo = PeopleViewer.Properties.Resources.person;
            people[14].RegistrationNumber = 15;

            return people;
        }

        public static Person GetPersonByEmail(string email)
        {
            Person[] people = AllPeople();

            foreach (Person person in people)
            {
                if (person.Email == email)
                {
                    return person;
                }
            }
            return null;
        }

    }
}
