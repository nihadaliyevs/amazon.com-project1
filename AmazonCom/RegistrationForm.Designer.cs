﻿namespace AmazonCom
{
    partial class RegistrationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RegistrationForm));
            this.lbl_Name = new System.Windows.Forms.Label();
            this.lbl_email = new System.Windows.Forms.Label();
            this.lbl_password = new System.Windows.Forms.Label();
            this.lbl_confirm_Password = new System.Windows.Forms.Label();
            this.txtbx_name = new System.Windows.Forms.TextBox();
            this.txtxbx_email = new System.Windows.Forms.TextBox();
            this.txtbx_password = new System.Windows.Forms.TextBox();
            this.txtbx_passwordConfirm = new System.Windows.Forms.TextBox();
            this.lbl_nameError = new System.Windows.Forms.Label();
            this.lbl_emailError = new System.Windows.Forms.Label();
            this.lbl_passwordError = new System.Windows.Forms.Label();
            this.lbl_confirmPasswordError = new System.Windows.Forms.Label();
            this.btn_register = new System.Windows.Forms.Button();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_Name
            // 
            this.lbl_Name.AutoSize = true;
            this.lbl_Name.Location = new System.Drawing.Point(265, 64);
            this.lbl_Name.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbl_Name.Name = "lbl_Name";
            this.lbl_Name.Size = new System.Drawing.Size(385, 32);
            this.lbl_Name.TabIndex = 0;
            this.lbl_Name.Text = "Please enter your user name:";
            // 
            // lbl_email
            // 
            this.lbl_email.AutoSize = true;
            this.lbl_email.Location = new System.Drawing.Point(265, 231);
            this.lbl_email.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbl_email.Name = "lbl_email";
            this.lbl_email.Size = new System.Drawing.Size(321, 32);
            this.lbl_email.TabIndex = 1;
            this.lbl_email.Text = "Please enter your email:";
            // 
            // lbl_password
            // 
            this.lbl_password.AutoSize = true;
            this.lbl_password.Location = new System.Drawing.Point(265, 417);
            this.lbl_password.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbl_password.Name = "lbl_password";
            this.lbl_password.Size = new System.Drawing.Size(373, 32);
            this.lbl_password.TabIndex = 2;
            this.lbl_password.Text = "Please entre your password:";
            // 
            // lbl_confirm_Password
            // 
            this.lbl_confirm_Password.AutoSize = true;
            this.lbl_confirm_Password.Location = new System.Drawing.Point(265, 615);
            this.lbl_confirm_Password.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbl_confirm_Password.Name = "lbl_confirm_Password";
            this.lbl_confirm_Password.Size = new System.Drawing.Size(339, 32);
            this.lbl_confirm_Password.TabIndex = 3;
            this.lbl_confirm_Password.Text = "Please confirm password:";
            // 
            // txtbx_name
            // 
            this.txtbx_name.Location = new System.Drawing.Point(273, 103);
            this.txtbx_name.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.txtbx_name.Name = "txtbx_name";
            this.txtbx_name.Size = new System.Drawing.Size(567, 38);
            this.txtbx_name.TabIndex = 4;
            // 
            // txtxbx_email
            // 
            this.txtxbx_email.Location = new System.Drawing.Point(273, 269);
            this.txtxbx_email.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.txtxbx_email.Name = "txtxbx_email";
            this.txtxbx_email.Size = new System.Drawing.Size(567, 38);
            this.txtxbx_email.TabIndex = 5;
            // 
            // txtbx_password
            // 
            this.txtbx_password.Location = new System.Drawing.Point(273, 455);
            this.txtbx_password.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.txtbx_password.Name = "txtbx_password";
            this.txtbx_password.Size = new System.Drawing.Size(567, 38);
            this.txtbx_password.TabIndex = 6;
            // 
            // txtbx_passwordConfirm
            // 
            this.txtbx_passwordConfirm.Location = new System.Drawing.Point(273, 653);
            this.txtbx_passwordConfirm.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.txtbx_passwordConfirm.Name = "txtbx_passwordConfirm";
            this.txtbx_passwordConfirm.Size = new System.Drawing.Size(567, 38);
            this.txtbx_passwordConfirm.TabIndex = 7;
            // 
            // lbl_nameError
            // 
            this.lbl_nameError.AutoSize = true;
            this.lbl_nameError.Font = new System.Drawing.Font("Garamond", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_nameError.ForeColor = System.Drawing.Color.DarkRed;
            this.lbl_nameError.Location = new System.Drawing.Point(265, 157);
            this.lbl_nameError.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbl_nameError.Name = "lbl_nameError";
            this.lbl_nameError.Size = new System.Drawing.Size(0, 37);
            this.lbl_nameError.TabIndex = 8;
            // 
            // lbl_emailError
            // 
            this.lbl_emailError.AutoSize = true;
            this.lbl_emailError.Font = new System.Drawing.Font("Garamond", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_emailError.ForeColor = System.Drawing.Color.DarkRed;
            this.lbl_emailError.Location = new System.Drawing.Point(265, 324);
            this.lbl_emailError.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbl_emailError.Name = "lbl_emailError";
            this.lbl_emailError.Size = new System.Drawing.Size(0, 37);
            this.lbl_emailError.TabIndex = 9;
            // 
            // lbl_passwordError
            // 
            this.lbl_passwordError.AutoSize = true;
            this.lbl_passwordError.Font = new System.Drawing.Font("Garamond", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_passwordError.ForeColor = System.Drawing.Color.DarkRed;
            this.lbl_passwordError.Location = new System.Drawing.Point(265, 510);
            this.lbl_passwordError.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbl_passwordError.Name = "lbl_passwordError";
            this.lbl_passwordError.Size = new System.Drawing.Size(0, 37);
            this.lbl_passwordError.TabIndex = 10;
            // 
            // lbl_confirmPasswordError
            // 
            this.lbl_confirmPasswordError.AutoSize = true;
            this.lbl_confirmPasswordError.Font = new System.Drawing.Font("Garamond", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_confirmPasswordError.ForeColor = System.Drawing.Color.DarkRed;
            this.lbl_confirmPasswordError.Location = new System.Drawing.Point(265, 708);
            this.lbl_confirmPasswordError.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbl_confirmPasswordError.Name = "lbl_confirmPasswordError";
            this.lbl_confirmPasswordError.Size = new System.Drawing.Size(0, 37);
            this.lbl_confirmPasswordError.TabIndex = 11;
            // 
            // btn_register
            // 
            this.btn_register.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_register.Font = new System.Drawing.Font("Arvo", 9.900001F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_register.Location = new System.Drawing.Point(273, 730);
            this.btn_register.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btn_register.Name = "btn_register";
            this.btn_register.Size = new System.Drawing.Size(260, 95);
            this.btn_register.TabIndex = 12;
            this.btn_register.Text = "Register";
            this.btn_register.UseVisualStyleBackColor = true;
            this.btn_register.Click += new System.EventHandler(this.btn_register_Click);
            // 
            // btn_cancel
            // 
            this.btn_cancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_cancel.Font = new System.Drawing.Font("Arvo", 9.900001F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_cancel.Location = new System.Drawing.Point(580, 730);
            this.btn_cancel.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(260, 95);
            this.btn_cancel.TabIndex = 13;
            this.btn_cancel.Text = "Cancel";
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // RegistrationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1131, 1073);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.btn_register);
            this.Controls.Add(this.lbl_confirmPasswordError);
            this.Controls.Add(this.lbl_passwordError);
            this.Controls.Add(this.lbl_emailError);
            this.Controls.Add(this.lbl_nameError);
            this.Controls.Add(this.txtbx_passwordConfirm);
            this.Controls.Add(this.txtbx_password);
            this.Controls.Add(this.txtxbx_email);
            this.Controls.Add(this.txtbx_name);
            this.Controls.Add(this.lbl_confirm_Password);
            this.Controls.Add(this.lbl_password);
            this.Controls.Add(this.lbl_email);
            this.Controls.Add(this.lbl_Name);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.Name = "RegistrationForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Registration";
            this.Load += new System.EventHandler(this.RegistrationForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_Name;
        private System.Windows.Forms.Label lbl_email;
        private System.Windows.Forms.Label lbl_password;
        private System.Windows.Forms.Label lbl_confirm_Password;
        private System.Windows.Forms.TextBox txtbx_name;
        private System.Windows.Forms.TextBox txtxbx_email;
        private System.Windows.Forms.TextBox txtbx_password;
        private System.Windows.Forms.TextBox txtbx_passwordConfirm;
        private System.Windows.Forms.Label lbl_nameError;
        private System.Windows.Forms.Label lbl_emailError;
        private System.Windows.Forms.Label lbl_passwordError;
        private System.Windows.Forms.Label lbl_confirmPasswordError;
        private System.Windows.Forms.Button btn_register;
        private System.Windows.Forms.Button btn_cancel;
    }
}

