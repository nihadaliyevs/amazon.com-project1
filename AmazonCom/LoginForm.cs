﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AmazonCom
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private void btn_LogIn_Click(object sender, EventArgs e)
        {
            lbl_EmailError.Text = "";
            lbl_passwordError.Text = "";
            bool isvalidationOk = true;

            if (string.IsNullOrWhiteSpace(txbx_email.Text) || txbx_email.Text.Length < 2|| !txbx_email.Text.Contains("@")) 
            {
                lbl_EmailError.Text = "Your email adress is not valid";
                isvalidationOk = false;
            }
            if (string.IsNullOrWhiteSpace(txbx_password.Text) || txbx_password.Text.Length < 6) 
            {
                lbl_passwordError.Text = "Your password is not valid";
                isvalidationOk = false;
            }

            if (isvalidationOk)
            {
                bool hasgivenUser= Users.FindUser(txbx_email.Text, txbx_password.Text);
                if (hasgivenUser)
                {
                    Hide();
                    MainForm mainForm = new MainForm();
                    mainForm.Show();
                }
                else
                {
                    MessageBox.Show("Login is failed");
                }
           
            }

        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            Hide();
            GreetingForm greetingForm = new GreetingForm();
            greetingForm.ShowDialog();
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {

        }
    }
}
