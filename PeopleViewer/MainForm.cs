﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PeopleViewer
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            Person[] people = People.AllPeople() ;
            int x = 47;
            int y = 39;
            int namey = 190;
            int surnamey = 215;
            int emaily = 238;
            int counter = 0;
            int buttonNumber = 1;
            int buttonX = 260;
            int buttonY = 500;
            int btnName = 1;


            for (int i = 0; i < people.Length; i++)
            {
                if (i!=0 && i% 3 == 0)
                {
                    x = 47;
                    y += 230;
                    namey += 235;
                    surnamey += 233;
                    emaily += 233;

                }

                PictureBox pictureBox = new PictureBox();
                pictureBox.Size = new Size(174, 144);
                pictureBox.Location = new Point(x, y);
                pictureBox.Image = PeopleViewer.Properties.Resources.person;
                pictureBox.SizeMode = PictureBoxSizeMode.StretchImage;

                Label labelName = new Label();
                labelName.Location = new Point(x, namey);
                labelName.Text = $"Name: {people[i].Name}";

                Label labelSurname = new Label();
                labelSurname.Location = new Point(x, surnamey);
                labelSurname.Text = $"Surname: {people[i].Surname}";
                labelSurname.Size = new Size(200, 20);

                Label labelEmail = new Label();
                labelEmail.Location = new Point(x, emaily);
                labelEmail.Text = $"Email: {people[i].Email}";
                labelEmail.Size = new Size(200, 20);

                x +=240 ;
                counter++;
                if (people[i].RegistrationNumber < 7)
                {
                    Controls.Add(pictureBox);
                    Controls.Add(labelName);
                    Controls.Add(labelSurname);
                    Controls.Add(labelEmail);
                }
                
                if (counter!=0 && counter % 6==1)
                {
                    Button button = new Button();
                    button.Location = new Point(buttonX, buttonY);
                    button.Size = new Size(23, 24);
                    button.Text = buttonNumber.ToString();
                    button.Name = $"btn_{btnName.ToString()}";

                    buttonX += 30;
                    buttonNumber++;

                    Controls.Add(button);
                    btnName++;
                }

            }

        }

        private void btn_1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("I'm Clicked");
        }
    }
}
