﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazonCom
{
    public class User
    {
        public User(string name, string email, string password)
        {
            Name = name;
            Email = email;
            Password = password;
        }

        private string _name;
        private string _email;
        private string _password;

        public String Name
        {
            set { if (value.Length> 2) _name = value; }
            get { return _name; }
        }

        public string Email
        {
            set { if (value.Contains("@") && value.Length>6) _email=value; }
            get { return _email; }
        }
        public string Password
        {
            set { if (value.Length <= 6) _password=value ; }
            get { return _password; }
        }
    }
}
