﻿namespace Calculator
{
    partial class RegisterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtbx_FirstNumber = new System.Windows.Forms.TextBox();
            this.lbl_FirstNumber = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtbx_SecondNumber = new System.Windows.Forms.TextBox();
            this.btn_Calculate = new System.Windows.Forms.Button();
            this.cmbx_Operation = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // txtbx_FirstNumber
            // 
            this.txtbx_FirstNumber.Location = new System.Drawing.Point(265, 74);
            this.txtbx_FirstNumber.Name = "txtbx_FirstNumber";
            this.txtbx_FirstNumber.Size = new System.Drawing.Size(408, 38);
            this.txtbx_FirstNumber.TabIndex = 0;
            this.txtbx_FirstNumber.TextChanged += new System.EventHandler(this.txtbx_FirstNumber_TextChanged);
            // 
            // lbl_FirstNumber
            // 
            this.lbl_FirstNumber.AutoSize = true;
            this.lbl_FirstNumber.Location = new System.Drawing.Point(265, 36);
            this.lbl_FirstNumber.Name = "lbl_FirstNumber";
            this.lbl_FirstNumber.Size = new System.Drawing.Size(378, 32);
            this.lbl_FirstNumber.TabIndex = 1;
            this.lbl_FirstNumber.Text = "Please enter the first number";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(265, 143);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(378, 32);
            this.label1.TabIndex = 3;
            this.label1.Text = "Please enter the first number";
            // 
            // txtbx_SecondNumber
            // 
            this.txtbx_SecondNumber.Location = new System.Drawing.Point(265, 181);
            this.txtbx_SecondNumber.Name = "txtbx_SecondNumber";
            this.txtbx_SecondNumber.Size = new System.Drawing.Size(408, 38);
            this.txtbx_SecondNumber.TabIndex = 2;
            // 
            // btn_Calculate
            // 
            this.btn_Calculate.Location = new System.Drawing.Point(271, 365);
            this.btn_Calculate.Name = "btn_Calculate";
            this.btn_Calculate.Size = new System.Drawing.Size(349, 120);
            this.btn_Calculate.TabIndex = 4;
            this.btn_Calculate.Text = "Calculate";
            this.btn_Calculate.UseVisualStyleBackColor = true;
            this.btn_Calculate.Click += new System.EventHandler(this.btn_Calculate_Click);
            // 
            // cmbx_Operation
            // 
            this.cmbx_Operation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbx_Operation.FormattingEnabled = true;
            this.cmbx_Operation.Items.AddRange(new object[] {
            "+",
            "-",
            "*",
            "/"});
            this.cmbx_Operation.Location = new System.Drawing.Point(265, 265);
            this.cmbx_Operation.Name = "cmbx_Operation";
            this.cmbx_Operation.Size = new System.Drawing.Size(408, 39);
            this.cmbx_Operation.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(899, 942);
            this.Controls.Add(this.cmbx_Operation);
            this.Controls.Add(this.btn_Calculate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtbx_SecondNumber);
            this.Controls.Add(this.lbl_FirstNumber);
            this.Controls.Add(this.txtbx_FirstNumber);
            this.Name = "Form1";
            this.Text = "Caclulator";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtbx_FirstNumber;
        private System.Windows.Forms.Label lbl_FirstNumber;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtbx_SecondNumber;
        private System.Windows.Forms.Button btn_Calculate;
        private System.Windows.Forms.ComboBox cmbx_Operation;
    }
}

