﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AliExpressCom
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private void btn_LogIn_Click(object sender, EventArgs e)
        {
            lbl_error.Text = "";
            lbl_passwordError.Text = "";

            bool isValidationOK = true;
            if (String.IsNullOrWhiteSpace(txtbx_Email.Text) || txtbx_Email.Text.Length<6||!txtbx_Email.Text.Contains("@"))
            {
                isValidationOK = false;
                lbl_error.Text = "Email is in correct";
            }
            if (string.IsNullOrWhiteSpace(txtbx_password.Text) || txtbx_password.Text.Length < 6)
            {
                isValidationOK = false;
                lbl_passwordError.Text = "Sorry, password is incorrect";
            }

            if (isValidationOK)
            {
                bool isFound = Users.FindUser(txtbx_Email.Text, txtbx_password.Text);
                if (isFound)
                {
                    MessageBox.Show("Log in is successfull");
                }
                else
                {
                    MessageBox.Show("Sorry, Log in failed");
                }
            }

            
        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            Hide();
            GreetingForm greetingForm = new GreetingForm();
            greetingForm.ShowDialog();
        }
    }
}
